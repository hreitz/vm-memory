use crate::bitmap::{Bitmap, BitmapSlice, WithBitmapSlice};
use crate::mmap::{BitmapMmap, NewBitmap};
use std::fs::File;
use std::os::fd::AsRawFd;
use std::sync::atomic::{AtomicU8, Ordering};
use std::{io, ptr, slice};

#[derive(Default, Debug)]
#[allow(missing_docs)]
pub struct AtomicBitmapMmap {
    bitmap: &'static [AtomicU8],
    size: u64,
    page_size: usize,
    offset: usize,
}

impl Clone for AtomicBitmapMmap {
    fn clone(&self) -> Self {
        Self::default()
    }
}

impl Drop for AtomicBitmapMmap {
    fn drop(&mut self) {
        let map_size = (self.size + self.offset as u64 + 0xfff) & !0xfff;

        unsafe {
            libc::munmap(
                (self.bitmap.as_ptr() as usize - self.offset) as *mut libc::c_void,
                map_size as libc::size_t,
            );
        }
    }
}

fn page_block(page: usize) -> usize {
    // Each page is indexed inside a u8, so we need to divide by 8
    page >> 3
}

fn page_bit(page: usize) -> usize {
    // The bit index inside a block of 8 bits (i.e., page % 8)
    page & 7
}

impl BitmapMmap for AtomicBitmapMmap {
    fn from_shm_file(file: File, offset: u64, size: u64) -> io::Result<Self> {
        let map_offset = offset & !0xfff;
        let offset: usize = (offset - map_offset).try_into().unwrap();

        let map_size = (size + offset as u64 + 0xfff) & !0xfff;

        let addr = unsafe {
            libc::mmap(
                ptr::null_mut(),
                map_size as libc::size_t,
                libc::PROT_READ | libc::PROT_WRITE,
                libc::MAP_SHARED,
                file.as_raw_fd(),
                map_offset as libc::off_t,
            )
        };

        if addr == libc::MAP_FAILED {
            return Err(io::Error::last_os_error());
        }

        let bitmap = unsafe {
            slice::from_raw_parts_mut((addr as usize + offset) as *mut AtomicU8, size as usize)
        };
        Ok(Self {
            bitmap,
            size,
            page_size: 0x1000,
            offset,
        })
    }
}

impl BitmapSlice for AtomicBitmapMmap {}

impl<'a> WithBitmapSlice<'a> for AtomicBitmapMmap {
    type S = Self;
}

impl Bitmap for AtomicBitmapMmap {
    fn mark_dirty(&self, offset: usize, len: usize) {
        if self.bitmap.is_empty() || len == 0 {
            return;
        }

        let first_page = offset / self.page_size;
        let last_page = offset.saturating_add(len - 1) / self.page_size;
        for n in first_page..=last_page {
            if n >= self.size as usize {
                // Attempts to set bits beyond the end of the bitmap are simply ignored.
                break;
            }
            self.bitmap[page_block(n)].fetch_or(1 << page_bit(n), Ordering::SeqCst);
        }
    }

    fn dirty_at(&self, offset: usize) -> bool {
        let page = offset / self.page_size;
        if page < self.size as usize {
            (self.bitmap[page_block(page)].load(Ordering::Acquire) & (1 << page_bit(page))) != 0
        } else {
            // Out-of-range bits are always unset.
            false
        }
    }

    fn slice_at(&self, _offset: usize) -> <Self as WithBitmapSlice>::S {
        // FIXME: this is not correct, we need to create a new mmap
        self.clone()
    }
}

impl NewBitmap for AtomicBitmapMmap {
    fn with_len(_len: usize) -> Self {
        Self::default()
    }
}
